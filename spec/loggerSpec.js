const Logger = imports.logger;

const LOG_ERROR = 250;
const LOG_INFO = 150;
const LOG_DETAIL = 100;
const LOG_DEBUG = 50;
const LOG_FLOW = 0;

/* eslint no-magic-numbers: "off" */
describe('a logger', () => {
    beforeEach(() => {
        this.testLogger = new Logger.Logger();
        this.testLogger.addLevel('error', '[ERROR ]', LOG_ERROR);
        this.testLogger.addLevel('info', '[INFO  ]', LOG_INFO);
        this.testLogger.addLevel('detail', '[DETAIL]', LOG_DETAIL);
        this.testLogger.addLevel('debug', '[DEBUG ]', LOG_DEBUG);
        this.testLogger.addLevel('flow', '[FLOW  ]', LOG_FLOW);
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        const testLogFunction = (message) => {
            this.nbOfLogCalls++;
            this.passedMessage = message;
        };
        this.testLogger._log = testLogFunction;
    });

    it('has sensible defaults', () => {
        expect(this.testLogger._level).toBe(LOG_ERROR);
        expect(this.testLogger._prefix).toBe('');
        expect(this.testLogger._addNewLine).toBeTruthy();
    });

    it('has a configurable prefix', () => {
        this.testLogger.prefix = 'prefix';

        expect(this.testLogger._prefix).toBe('prefix ');
        this.testLogger.prefix = '';

        expect(this.testLogger._prefix).toBe('');
        this.testLogger.prefix = null;

        expect(this.testLogger._prefix).toBe('');
        this.testLogger.prefix = undefined;

        expect(this.testLogger._prefix).toBe('');
    });

    it('has a configurable level', () => {
        this.testLogger.level = LOG_DEBUG;

        expect(this.testLogger._level).toBe(LOG_DEBUG);
    });

    it('can be configured to add newlines', () => {
        expect(this.testLogger._addNewLine).toBeTruthy();
        this.testLogger.addNewLine = true;

        expect(this.testLogger._addNewLine).toBeTruthy();
        this.testLogger.addNewLine = false;

        expect(this.testLogger._addNewLine).toBeFalsy();
        this.testLogger.addNewLine = false;

        expect(this.testLogger._addNewLine).toBeFalsy();
        this.testLogger.addNewLine = true;

        expect(this.testLogger._addNewLine).toBeTruthy();
        this.testLogger.addNewLine = undefined;

        expect(this.testLogger._addNewLine).toBeTruthy();
        this.testLogger.addNewLine = false;

        expect(this.testLogger._addNewLine).toBeFalsy();
        this.testLogger.addNewLine = null;

        expect(this.testLogger._addNewLine).toBeFalsy();
    });

    it('logs with or without adding a new line depending on the configuration', () => {
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('TestMessage\r\n');

        this.testLogger.addNewLine = false;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('TestMessage');

        this.testLogger.addNewLine = true;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('TestMessage\r\n');
    });

    it('only shows messages that are above or equal to the configured level', () => {
        this.testLogger.prefix = '[test]';

        // Level Error
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage2', LOG_INFO);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage3', LOG_DETAIL);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage4', LOG_DEBUG);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage5', LOG_FLOW);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');

        // Level info
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_INFO;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage2', LOG_INFO);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage3', LOG_DETAIL);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage4', LOG_DEBUG);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage5', LOG_FLOW);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');

        // Level detail
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_DETAIL;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage2', LOG_INFO);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage3', LOG_DETAIL);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] TestMessage3\r\n');
        this.testLogger.log('TestMessage4', LOG_DEBUG);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] TestMessage3\r\n');
        this.testLogger.log('TestMessage5', LOG_FLOW);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] TestMessage3\r\n');

        // Level debug
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_DEBUG;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage2', LOG_INFO);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage3', LOG_DETAIL);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] TestMessage3\r\n');
        this.testLogger.log('TestMessage4', LOG_DEBUG);

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] TestMessage4\r\n');
        this.testLogger.log('TestMessage5', LOG_FLOW);

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] TestMessage4\r\n');


        // Level flow
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_FLOW;
        this.testLogger.log('TestMessage', LOG_ERROR);

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] TestMessage\r\n');
        this.testLogger.log('TestMessage2', LOG_INFO);

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] TestMessage2\r\n');
        this.testLogger.log('TestMessage3', LOG_DETAIL);

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] TestMessage3\r\n');
        this.testLogger.log('TestMessage4', LOG_DEBUG);

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] TestMessage4\r\n');
        this.testLogger.log('TestMessage5', LOG_FLOW);

        expect(this.nbOfLogCalls).toBe(5);
        expect(this.passedMessage).toBe('[test] TestMessage5\r\n');


    });

    it('has dedicated functions to log with a specific level', () => {
        this.testLogger.prefix = '[test]';

        // Level Error
        this.testLogger.error('TestMessage');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.info('TestMessage2');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.detail('TestMessage3');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.debug('TestMessage4');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.flow('TestMessage5');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');

        // Level info
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_INFO;
        this.testLogger.error('TestMessage');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.info('TestMessage2');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.detail('TestMessage3');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.debug('TestMessage4');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.flow('TestMessage5');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');

        // Level detail
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_DETAIL;
        this.testLogger.error('TestMessage');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.info('TestMessage2');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.detail('TestMessage3');

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] [DETAIL] TestMessage3\r\n');
        this.testLogger.debug('TestMessage4');

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] [DETAIL] TestMessage3\r\n');
        this.testLogger.flow('TestMessage5');

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] [DETAIL] TestMessage3\r\n');

        // Level debug
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_DEBUG;
        this.testLogger.error('TestMessage');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.info('TestMessage2');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.detail('TestMessage3');

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] [DETAIL] TestMessage3\r\n');
        this.testLogger.debug('TestMessage4');

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] [DEBUG ] TestMessage4\r\n');
        this.testLogger.flow('TestMessage5');

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] [DEBUG ] TestMessage4\r\n');

        // Level flow
        this.nbOfLogCalls = 0;
        this.passedMessage = '';
        this.testLogger.level = LOG_FLOW;
        this.testLogger.error('TestMessage');

        expect(this.nbOfLogCalls).toBe(1);
        expect(this.passedMessage).toBe('[test] [ERROR ] TestMessage\r\n');
        this.testLogger.info('TestMessage2');

        expect(this.nbOfLogCalls).toBe(2);
        expect(this.passedMessage).toBe('[test] [INFO  ] TestMessage2\r\n');
        this.testLogger.detail('TestMessage3');

        expect(this.nbOfLogCalls).toBe(3);
        expect(this.passedMessage).toBe('[test] [DETAIL] TestMessage3\r\n');
        this.testLogger.debug('TestMessage4');

        expect(this.nbOfLogCalls).toBe(4);
        expect(this.passedMessage).toBe('[test] [DEBUG ] TestMessage4\r\n');
        this.testLogger.flow('TestMessage5');

        expect(this.nbOfLogCalls).toBe(5);
        expect(this.passedMessage).toBe('[test] [FLOW  ] TestMessage5\r\n');
    });
});
